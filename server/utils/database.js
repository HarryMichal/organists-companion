'use strict';

const Database = require('better-sqlite3');

const db = new Database('./db/database.db', { verbose: console.log, fileMustExist: true });

const database = {
  run(sql, parameters = []) {
    return new Promise((resolve, reject) => {
      try {
        const info = db.prepare(sql).run(...parameters);
        resolve(info);
      }
      catch(err) {
        reject(err);
      }
    })
  },

  get(sql, parameters = []) {
    return new Promise((resolve, reject) => {
      try {
        const row = db.prepare(sql).get(...parameters);
        resolve(row);
      }
      catch (err) {
        reject(err);
      }
    })
  },

  all(sql, parameters = []) {
    return new Promise((resolve, reject) => {
      try {
        const rows = db.prepare(sql).all(...parameters);
        resolve(rows);
      }
      catch (err) {
        reject(err);
      }
    })
  },

  selectOne(items, source, search, searched) {
    return new Promise((resolve, reject) => {
      try {
        const query = db.prepare(`SELECT ${items} FROM ${source} WHERE ${search} =?`);
        const selectedItem = query.get(searched);
        
        if (selectedItem) {
          resolve(selectedItem);
        }
        else {
          resolve(false);
        }
      }
      catch(err) {
        reject(err)
      }
    })
  },

  selectAll(item, source, orderBy) {
    return new Promise((resolve, reject) => {
      try {
        let query = db.prepare(`SELECT ${item} FROM ${source} ORDER BY ${orderBy}`)
        let selectedRows = query.all();

        if (selectedRows.length > 0) {
          resolve(selectedRows)
        }
        else {
          resolve(false);
        }
      }
      catch(err) {
        reject(err)
      }
    })
  },

  updateOne(destination, items, search, searched) {
    return new Promise((resolve, reject) => {
      try {
        let query = db.prepare(`UPDATE ${destination} SET ${items} WHERE ${search}`);
        let info = query.run(searched)
        
        if (info.changes === 1)
          resolve(true)
        else
          resolve(false)
      }
      catch(err) {
        reject(err)
      }
    })
  },

  updateMore(destination, items, search, searched) {
    return new Promise((resolve, reject) => {
      try {
        let query = db.prepare(`UPDATE ${destination} SET ${items} WHERE ${search}`);
        let info = query.run(searched)

        if (info.changes === 1)
          resolve(true)
        else
          resolve(false)
      } catch (err) {
        reject(err)
      }
    })
  },

  insertMore(destination, columns, values) {
    return new Promise((resolve, reject) => {
      try {
        let placeholders = values.map(() => '?').join(',');

        let query = db.prepare(`INSERT INTO ${destination} (${columns}) VALUES (${placeholders})`);
        let info = query.run(values);

        if (info.changes >= 1)
          resolve(info.changes)
        else
          resolve(false)
      }
      catch(err) {
        reject(err)
      }
    })
  }
}

module.exports = database;
